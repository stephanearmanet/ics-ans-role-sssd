import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('default_group')


def test_sssd_running_and_enabled(Service):
    # Testing that the service is running shows that
    # there is no obvious configuration error
    # It would be nice to test that LDAP authentication works
    # (it would require using the real password)...
    sssd = Service("sssd")
    assert sssd.is_running
    assert sssd.is_enabled


def test_oddjobd_running_and_enabled(Service):
    oddjobd = Service("oddjobd")
    assert oddjobd.is_running
    assert oddjobd.is_enabled


def test_ldap_conf(File):
    ldap_conf = File('/etc/openldap/ldap.conf').content_string
    assert re.search(r'\nTLS_CACERTDIR\s+/etc/openldap/certs', ldap_conf) is not None


def test_pam_conf(File):
    pam_conf = File('/etc/pam.d/system-auth').content_string
    assert re.search(r'\nauth\s+sufficient\s+pam_sss.so', pam_conf) is not None
