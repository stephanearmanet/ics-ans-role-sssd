import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('disabled_group')


def test_sssd_stopped_and_disabled(Service):
    sssd = Service("sssd")
    assert not sssd.is_running
    assert not sssd.is_enabled


def test_oddjobd_stopped_and_disabled(Service):
    oddjobd = Service("oddjobd")
    assert not oddjobd.is_running
    assert not oddjobd.is_enabled


def test_pam_conf(File):
    pam_conf = File('/etc/pam.d/system-auth').content_string
    assert re.search(r'\nauth\s+sufficient\s+pam_sss.so', pam_conf) is None
